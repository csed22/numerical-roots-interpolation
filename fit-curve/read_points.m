function [X, Y] = read_points ()
  
  %https://www.mathworks.com/matlabcentral/answers/38630-how-read-array-from-a-text-file
  file = dlmread('data.txt');
  
  X = file(:,1);
  Y = file(:,2);
  
endfunction
