function xr = simple_false_position (f, xu, xl, EPSILON)
  
  if (f(xl) * f(xu))>0 % This if should never occur !!
    disp('No root enclosed, Incorrect initial guesses!');
    return
  endif
  
  do
    
    xr =( (xl*f(xu) ) -  (xu*f(xl)) ) / ( f(xu)-f(xl) );       % compute the midpoint xr
    ea = abs((xr-xl)/xr) * 100;                                % approx. relative error
    test = f(xl) * f(xr);                                      % compute f(xl)*f(xr)
    
    if (test < 0) 
      xu = xr;
    else 
      xl = xr;
    endif
    
    if (test == 0) 
      ea = 0; 
    endif
    
  until (ea <= EPSILON)

endfunction
