function retval = input_point()
  
  prompt = {"Query point"};
  defaults = {"50"};
  rowscols = [1,20];
  
  inputArr = inputdlg (prompt, "Interpolation", rowscols, defaults);
  
  %%https://www.mathworks.com/matlabcentral/answers/342949-cell-to-number-array
  retval = str2double(inputArr(1){:});

endfunction
