## Bad euqation after 20 I/P

function [solution,func] = newton_interpolation(x,y,p)

%---------------------------------------------------------------------------
%          The method is Newton interpolation divided difference.
% Inputs
%   po ==> polynomial order
%   x ==> sample points
%   y ==> corresponding sample points
%   p ==> query point(s). 
% Return
%   solution ==> corresponding value to p
%   func ==> string of the function itself
%   t==> Execution time
%    Function's plot
%---------------------------------------------------------------------------

  [x, y] = read_points();
  p = input_point();
  
  n = length(x);
  po = n - 1;
  
  x =x';
  y =y';

  disp("");
  
  tic;

  D = zeros(n,n);
  D(:,1) = y';
  for j=2:n,
    for k=j:n,
        D(k,j) = (D(k,j-1)-D(k-1,j-1))/(x(k)-x(k-j+1));
    end
  end

  C = D(n,n);
  for k=(n-1):-1:1,
    C = conv(C,poly(x(k)));
    m = length(C);
    C(m) = C(m) + D(k,k);
  end

  for j = 1 : n
     a(j) = D(j, j);
  end
  Df(1) = 1;
  c(1) = a(1);
  for j = 2 : n
     Df(j)=(p - x(j-1)) .* Df(j-1);
     c(j) = a(j) .* Df(j);
  end
  
  solution = sum(c);

  %h = [min(x):0.1:max(x)];
  %g = C(length(C));

  func = '';
  for(i=length(C)-1 :-1: 1)
      if(C(length(C)-i)~=0)
          %if(C(length(C)-i)~=1)
              func = strcat(func,num2str(C(length(C)-i)));
          %end
          if(i~=1)
              func = strcat(func,'x^');
              func = strcat(func,num2str(i));
          else
              func = strcat(func,'x');
          end
          func = strcat(func,'+');
      end
      %g = g + C(length(C)-i)*h.^i;
  end
  
  func = strrep(strcat(func,num2str(C(length(C)))),"+-","-");
  func = strrep(func,"x","*x");
  func = strcat("@(x) ", func);
  f = str2func(func);
  
  toc;
  
  disp("");

  all_Xs = [x, p];
   
  custom_fplot(f,min(all_Xs),max(all_Xs));
  
  plot(x,y,'.b','markersize',20);
  plot(p,solution,'.r','markersize',20);
  hold off;
  
  %solution = f(p)
  solution = sum(c) %<- 100% accurate !!
endfunction
