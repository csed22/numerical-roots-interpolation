## Assignment - Part 2

function fit_curve

  %https://www.mathworks.com/help/matlab/matlab_prog/suppress-warnings.html
  warning('off','all');

  my_options = {"Newton Interpolation", "Lagrange Interpolation"};
                
  [sel, ok] = listdlg ("PromptString",{"Make sure to update the data file.","Select a method:"},
                       "SelectionMode", "single",
                       "ListString", my_options);
  switch sel
    case 1
      newton_interpolation();
    case 2
      lagrange_interpolation();
    otherwise
        if (ok == 1)
          disp ("Unkown selection!");
        else
          disp ("You cancelled.");
        endif
  endswitch   
    
endfunction
