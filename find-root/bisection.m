## Indentation corrected.
## Input Dialog Added.
## Commented some uneccesary code.

function [Root,vecXl,vecXr,Err] = bisection();

  #Input dialong for Bracketing Methods

  prompt = {"Function y =", "Lower guess", "Upper guess", "Max Iterations", "Epsilon"};
  defaults = {"x^2-3", "0", "2", "50", "0.00001"};
  rowscols = [1,30; 1,30; 1,30; 1,20; 1,20];
  
  inputArr = inputdlg (prompt, "Enter the method parameters", rowscols, defaults);

  %%https://www.mathworks.com/help/matlab/ref/str2func.html
  %%https://www.mathworks.com/help/matlab/ref/strcat.html#d120e1183754
  %%https://stackoverflow.com/questions/13258508/how-to-convert-a-cell-to-string-in-matlab
  f = str2func(strcat('@(x)', inputArr(1){:}));
  
  %%https://www.mathworks.com/matlabcentral/answers/342949-cell-to-number-array
  xl = str2double(inputArr(2){:});
  xu = str2double(inputArr(3){:});
  
  MAX_ITERATIONS = str2double(inputArr(4){:});
  EPSILON = str2double(inputArr(5){:});
  
  %%pause
  
  mDataxr = [];
  myfnxr = [];
  mDataxl = [];
  myfnxl = [];
  mDataErr = [];
  
  original_xl=xl;original_xu=xu;
  
  %xlabel('X Axis', 'FontSize', 14, 'FontWeight', 'bold');
  %ylabel('Y Axis', 'FontSize', 9, 'FontWeight', 'bold');
  
  figure(1)
  fplot(f,[xl,xu],'b');
  grid on;
  hold on;
  
  %https://www.mathworks.com/matlabcentral/answers/151308-how-to-make-xaxis-y-0-black-in-grid
  xx = [xl, xu];
  yy = [0, 0];
  plot(xx,yy,'k', 'LineWidth', 2)     % 'k' for black, and you can adjust the linewidth accordingly.
  
  if (f(xl) * f(xu)) > 0                % if guesses do not bracket, exit
    disp('No root enclosed, Incorrect initial guesses!');
    return
  endif
  
  disp('');

  %https://www.mathworks.com/help/matlab/ref/waitbar.html
  w = waitbar(0);
  %https://www.mathworks.com/matlabcentral/answers/1550-execution-time
  tic;
  
  boundaries = count_digits(MAX_ITERATIONS, EPSILON);
  MAX_DIGITS = boundaries(1);
  PRECISION = boundaries(2);
  
  for i=1:1:MAX_ITERATIONS
    
    waitbar(i/MAX_ITERATIONS, w);
    
    xr=(xu+xl)/2;                     % compute the midpoint xr
    ea=abs((xr-xl)/xr) * 100;         % approx. relative error
    test= f(xl) * f(xr);              % compute f(xl)*f(xr)
    
    %https://www.mathworks.com/help/matlab/ref/sprintf.html
    formatSpec = strcat(' %0', num2str(MAX_DIGITS), 'd - xl= %0.', num2str(PRECISION),'f ');
    s = sprintf(formatSpec ,i,xl);
    fprintf(s);
    
    mDataxl(i)=xl;
    myfnxl(i)=f(xl);
    
    if (test < 0) 
      xu=xr;
    else 
      xl=xr;
    endif
    
    if (test == 0) 
      ea=0;
    endif
    
    if ( ea < EPSILON || i == MAX_ITERATIONS ) 
      break;
    endif
  
    formatSpec = strcat(' xr = %0.', num2str(PRECISION),'f error = %0.', num2str(PRECISION),'f');
    s = sprintf(formatSpec, xr, ea);
    disp(s);
    
    mDataxr(i)=xr;
    myfnxr(i)=f(xr);
    mDataErr(i)=ea;
    
  endfor
  
  waitbar(1,w);
  close(w);

  mDataxr(i) = xr;
  myfnxr(i) = f(xr);
  mDataErr(i) = ea;
  
  formatSpec = strcat(' xr = %0.', num2str(PRECISION),'f error = %0.', num2str(PRECISION),'f');
  s = sprintf(formatSpec, xr, ea);
  disp(s);
  
  formatSpec = strcat('\nRoot= %0.', num2str(PRECISION),'f #Iterations = %d \n');
  s = sprintf(formatSpec, xr,i);
  disp(s);
  
  l = abs(original_xl-original_xu);
  k = abs(log2(l/EPSILON));   %disp(k);
  
  toc;
  if(k > MAX_ITERATIONS)
    disp(strcat(num2str(ceil(k-MAX_ITERATIONS)), " more iterations to reach the answer."))
  endif
  
  disp('');
  
  plot(mDataxr,myfnxr,'.','markersize',20);
  
  %xlabel('X Axis', 'FontSize', 14, 'FontWeight', 'bold');
  %ylabel('Y Axis', 'FontSize', 9, 'FontWeight', 'bold');
  
  %figure(1)
  %fplot(f,[original_xl,original_xu],'b');
  
  %grid on;
  %hold on;
  
  plot(mDataxl,myfnxl,'.','markersize',20);
  
  Root = xr ;
  vecXl = mDataxl;
  vecXr = mDataxr;
  Err = mDataErr;
  
endfunction
