## Helper function

function retval = count_digits (integer_num, double_num)
 
  %https://www.mathworks.com/matlabcentral/answers/10795-counting-the-number-of-digits
  integer_count = numel(num2str(integer_num));
    
  double_str = num2str(double_num);
  %https://www.mathworks.com/help/matlab/ref/strfind.html
  n = strfind(double_str,'e');
  
  if( length(n) == 0 )

    double_count = numel(num2str(double_str)) - 2;
    
  else
          
    %https://octave.sourceforge.io/octave/function/substr.html
    s = substr(double_str,n+1);
    double_count = abs(str2double(s));
    
  endif


  retval = [integer_count, double_count];

endfunction
