function fail_msg (num)
  
  formatSpec = '';
  
  if(num == 1)
    formatSpec = '\n%02d st Attempt failed!\n';
  elseif ( num == 2 )
    formatSpec = '\n%02d nd Attempt failed!\n';
  elseif ( num == 3 )
    formatSpec = '\n%02d rd Attempt failed!\n';
  else
    formatSpec = '\n%02d th Attempt failed!\n';
  endif
      
  s = sprintf(formatSpec, num);
  disp(s);
  
endfunction
