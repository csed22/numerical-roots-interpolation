## Ultra Slow because of the strings

function [solution,func] = lagrange_interpolation(x,y,point)
  
  [x, y] = read_points();
  point = input_point();
  
  pol_arr = {};
  solution = 0;
  polynomial = "";
  
  disp("");
  w = waitbar(0);
  tic;
  
  size = length(x);
  
  for i=1:size
    
    waitbar(i/size, w);
    
    %p=1;
    s="";
    z="";
    
    for j=1:size
     
      if(i!=j)
        %p=p*(p-x(j))/(x(i)-x(j));
        z = strcat(z , "(" , num2str(x(i)) , "-" , num2str(x(j)) , ")*");
        s = strcat(s , "(x-" , num2str(x(j)) , ")*");
      endif

  endfor
  
    s=strcat("(",substr(s,1,length(s)-1),")");
    z=strcat("(",substr(z,1,length(z)-1),")");
    s=strcat("(" , s , "/" , z , ")");

    polynomial=strcat(s , "*" , num2str(y(i)));
    %solution=solution+p*y(i)
    
    %pol_arr(i) = str2func(strcat("@(x) (",polynomial,")"))
    pol_arr(i) = strcat("(",polynomial,")");
    
  endfor  
  
  f_str = "";
    
  for i=1:length(pol_arr)
    f_str = strcat(f_str,pol_arr(i){:},"+");
  endfor
  
  func = str2func(strcat("@(x)",f_str,"0"));
  %solution = func(point);
  
  toc;
  
  waitbar(1,w);
  close(w);
  
  disp("");
  
  all_Xs = [x; point];
   
  custom_fplot(func,min(all_Xs),max(all_Xs));
  
  plot(x,y,'.b','markersize',20);
  plot(point,func(point),'.r','markersize',20);
  hold off;
  
  solution = func(point)

endfunction

## Important note, all code given below is TRASH
## fn handlers can be added as strings, the problem
## was missing brackets nothing more ...

%function [X, Y] = custom_fplot (multi_function, min_x, max_x)
%  
%  X = [];
%  Y = [];
%  
%  current = min_x;
%  step = (max_x-min_x) / 1000;
%  
%  while(current < max_x)
%  
%    X = [X, current];
%    Y = [Y, calculate(multi_function,current)];
%    current = current + step;    
%  
%  endwhile
%
%  plot(X,Y)
%  hold on;
%  grid on;
%  
%endfunction
%
%function retval = calculate (multi_function, value)
%
%  retval = 0;
%
%  for i=1:length(multi_function)
%    retval = retval + multi_function(i){:}(value);
%  endfor
%
%endfunction