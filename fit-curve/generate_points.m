function generate_points(num)
  
  %https://www.mathworks.com/help/matlab/ref/fscanf.html
  x = 100*rand(num,1);
  y = x.^3;
  
  data = [];
  
  for i=1:length(x)
    
    data = [data,x(i),y(i)];
    
  endfor
  
  fileID = fopen('data.txt','w');
  fprintf(fileID,'%0.50f\t%0.50f\n',data);
  fclose(fileID);

endfunction
