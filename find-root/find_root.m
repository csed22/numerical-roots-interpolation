## Assignment - Part 1

function find_root

  %https://www.mathworks.com/help/matlab/matlab_prog/suppress-warnings.html
  warning('off','all');

  my_options = {"Bisection", "False-position",...
                "Fixed point", "Newton-Raphson", "Secant", "General Solution"};
                
  [sel, ok] = listdlg ("PromptString",{"Select a method:"},
                       "SelectionMode", "single",
                       "ListString", my_options);
  
  switch sel
    case 1
      bisection();
    case 2
      false_position();
    case 3
      fixed_point();
    case 4
    
      s = ver('symbolic');
      
      if(length(s) > 0)
        disp ("\nLoading Symbolic package ...");
        pkg load symbolic;
        disp("");
        newton_raphson();
      else
        disp ("Symbolic package has to be installed!!");
      endif
          
    case 5
    
      s = ver('symbolic');
      
      if(length(s) > 0)
        disp ("\nLoading Symbolic package ...");
        pkg load symbolic;
        disp("");
        secant();
      else
        disp ("Symbolic package has to be installed!!");
      endif
      
    case 6
      
      s = ver('symbolic');
      
      if(length(s) > 0)
        disp ("\nLoading Symbolic package ...");
        pkg load symbolic;
        disp("");
        general_method();
      else
        disp ("Symbolic package has to be installed!!");
      endif
    
    otherwise
        if (ok == 1)
          disp ("Unkown selection!");
        else
          disp ("You cancelled.");
        endif
  endswitch   
    
endfunction
