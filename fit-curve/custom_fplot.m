function [X, Y] = custom_fplot (func, min_x, max_x)
  
  X = [];
  Y = [];
  
  current = min_x;
  step = (max_x-min_x) / 1000;
  
  while(current < max_x)
  
    X = [X, current];
    Y = [Y, func(current)];
    current = current + step;    
  
  endwhile

  plot(X,Y)
  hold on;
  grid on;
  
endfunction