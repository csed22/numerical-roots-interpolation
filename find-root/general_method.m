function retval = general_method()
  
  HE5O_MRAT = 1000;
  
  #Input dialong for Bracketing Methods

  prompt = {"Function y =", "Epsilon"};
  defaults = {"exp(x^2)-3*cos(x^5)+sin(exp(x^cos(x)))", "0.00001"};
  rowscols = [1,30; 1,20];
  
  inputArr = inputdlg (prompt, "Enter the method parameters", rowscols, defaults);

  %%https://www.mathworks.com/help/matlab/ref/str2func.html
  %%https://www.mathworks.com/help/matlab/ref/strcat.html#d120e1183754
  %%https://stackoverflow.com/questions/13258508/how-to-convert-a-cell-to-string-in-matlab
  f = str2func(strcat('@(x)', inputArr(1){:}));
  
  EPSILON = str2double(inputArr(2){:});
  
  %%pause

  pkg load symbolic; 
  syms x;  
  ff = f(x);
  ffd = diff(ff, x);
  df = function_handle(ffd);
  
  i = 0;
  
  tic;
  
  do
  
    retval = simple_newton_raphson (f, df, HE5O_MRAT, EPSILON);
    
    xu = retval(1);
    xl = retval(2);
    
    if (length(retval) < 3) 
      retval = xu;
      if f(xu) < EPSILON && isreal(xu) 
        break
      else
        i = i+1;
        fail_msg(i);       
        continue
      endif
    endif
    
    retval = simple_false_position (f, xu, xl, EPSILON);
    break 
    
  until ( i >= 20 )
  
  toc;
  
  if(i >= 20)
    disp("\nThere is no root!!\n");
    return;
  endif
    
  boundaries = count_digits(0, EPSILON);
  PRECISION = boundaries(2);
  
  formatSpec = strcat('\nRoot= %0.', num2str(PRECISION),'f\n');
  s = sprintf(formatSpec, retval);
  disp(s);
  
  step = 10^5 * EPSILON;
    
  figure(1)
  fplot(f,[retval-step, retval+step],'b');
  grid on;
  hold on;
  
  %https://www.mathworks.com/matlabcentral/answers/151308-how-to-make-xaxis-y-0-black-in-grid
  xx = [retval-step, retval+step];
  yy = [0, 0];
  plot(xx,yy,'k', 'LineWidth', 2)     % 'k' for black, and you can adjust the linewidth accordingly.
  
  plot(retval,f(retval),'.r','markersize',20);
  
endfunction
