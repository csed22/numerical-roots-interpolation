function [x_new,ea,iter,vec] = secant()
  
  #Input dialong for Secant Method

  prompt = {"Function y =", "Initial guess x0 =", "Initial guess x1 =", "Max Iterations", "Epsilon"};
  defaults = {"exp(-x)-x", "0", "1", "50", "0.00001"};
  rowscols = [1,30; 1,30; 1,30; 1,20; 1,20];
  
  inputArr = inputdlg (prompt, "Enter the method parameters", rowscols, defaults);

  %%https://www.mathworks.com/help/matlab/ref/str2func.html
  %%https://www.mathworks.com/help/matlab/ref/strcat.html#d120e1183754
  %%https://stackoverflow.com/questions/13258508/how-to-convert-a-cell-to-string-in-matlab
  f = str2func(strcat('@(x)', inputArr(1){:}));
  
  %%https://www.mathworks.com/matlabcentral/answers/342949-cell-to-number-array
  x0 = str2double(inputArr(2){:});
  x1 = str2double(inputArr(3){:});
  
  MAX_ITERATIONS = str2double(inputArr(4){:});
  EPSILON = str2double(inputArr(5){:});
  
  %%pause
  
  x_old=x0;
  x_older=x0;
  x_new=x1;
  iter=0;
  ea=0;
  vec=[];

  df = @(o,r) (f(o)-f(r))/(o-r);
  
  boundaries = count_digits(MAX_ITERATIONS, EPSILON);
  MAX_DIGITS = boundaries(1);
  PRECISION = boundaries(2);
  
  %https://www.mathworks.com/help/matlab/ref/waitbar.html
  w = waitbar(0);
  %https://www.mathworks.com/matlabcentral/answers/1550-execution-time
  tic;
  
  do
      waitbar(iter/MAX_ITERATIONS, w);
      
      x_older=x_old;
      x_old=x_new;
      ddf=df(x_old,x_older);

      x_new=x_old-(f(x_old)/ddf);
      vec=[vec , x_new];
      
      if (x_new!=0)
          ea = abs((x_new - x_old) / x_new) * 100;
      endif
      
      iter=iter+1;
      
  until (ea <= EPSILON || iter == MAX_ITERATIONS ) 
  
  waitbar(1,w);
  close(w);
  
  formatSpec = strcat('\nRoot= %0.', num2str(PRECISION),'f #Iterations = %d \n');
  s = sprintf(formatSpec, x_new,iter);
  disp(s);
  
  toc;
  disp("");
  
  %https://github.com/cbm755/octsympy/wiki/Notes-on-Windows-installation
  pkg load symbolic; 
  syms x;  
  ff = f(x);
  ffd = diff(ff, x);
  df = function_handle(ffd);
  
  figure(1)
  subplot (2, 2, 1)
  fplot(f,'b');
  ylabel('f(x)', 'FontSize', 10, 'FontWeight', 'bold');  

  subplot (2, 2, 2)
  fplot(df,'r');
  ylabel("f'(x)", 'FontSize', 10, 'FontWeight', 'bold'); 
  
  x_axis = @(x) 0;
  
  subplot (2, 1, 2)
  fplot(x_axis,'b')   %Fabrication
  
  hold on;
  fplot(x_axis,'k')
  fplot(df,'r');
  fplot(f,'b');
  
  plot(x_new,[0],'.','markersize',20);
  hold off;
  grid on;    

endfunction
