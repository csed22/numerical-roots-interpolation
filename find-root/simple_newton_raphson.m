function retval = simple_newton_raphson (f, df, MAX_ITERATIONS, EPSILON)

  x0 = exp(exp(exp(exp(rand) * rand * cos(pi * rand))));

  x_new = x0;
  x_old = x0;

  ea = 0;
  i = 0;
  
  do
    
      x_old = x_new;
      d = df(x_old);
      
      if (d != 0)
        x_new = x_old-(f(x_old)/d);
      endif
      
      if (f(x_old)*f(x_new) < 0);
        x = [x_new, x_old];
        retval = [max(x), min(x), i];
        return
      endif
      
      if (x_new != 0)
        ea = abs((x_new - x_old) / x_new) * 100;
      endif
      
      i = i+1;
      
  until (ea <= EPSILON || i == MAX_ITERATIONS)
  
  retval = [x_new, 0];

endfunction
